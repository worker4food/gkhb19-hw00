package gkhb19.android.hw00

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.net.Uri
import kotlinx.android.synthetic.main.resume.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val openUrl = { url: String ->
            Intent(Intent.ACTION_VIEW, Uri.parse(url)).also(::startActivity)
        }

        facebook_link.setOnClickListener { openUrl("https://facebook.com/rickandmorty") }
        twitter_link.setOnClickListener  { openUrl("https://twitter.com/rickandmorty") }
        imdb_link.setOnClickListener     { openUrl("https://www.imdb.com/title/tt2861424/") }
    }
}
